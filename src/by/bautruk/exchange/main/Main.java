package by.bautruk.exchange.main;

import by.bautruk.exchange.entity.Creator;
import by.bautruk.exchange.entity.Exchange;
import by.bautruk.exchange.entity.Trader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * @author ivan.bautruvkevich
 *         02.12.2015 19:05.
 */
public class Main {
    static {
        new DOMConfigurator().doConfigure("xml/log4j.xml", LogManager.getLoggerRepository());
    }

    static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        //create market
        Exchange market = Exchange.getInstance();
        //set time for trading
        market.setTradingTime(1000);

        //create traders
        Trader[] traders = new Trader[20];

        for (int i = 0; i < traders.length; i++) {
            traders[i] = new Trader(market, i, 5000, new Creator().createTraderStock());
            new Thread(traders[i]).start();
            logger.info(traders[i].toString());
        }

        logger.info(market.getIndex() + " - index for trading");
    }
}