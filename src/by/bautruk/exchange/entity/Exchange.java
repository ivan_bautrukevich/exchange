package by.bautruk.exchange.entity;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author ivan.bautruvkevich
 *         03.12.2015 21:21.
 */

public class Exchange {

    static Logger logger = Logger.getLogger(Exchange.class);

    private static double index;
    private static ReentrantLock lockExchange = new ReentrantLock();
    private static Lock lock = new ReentrantLock();
    private static Exchange instance;
    private static HashMap<CompanyEnum, Integer> applicationSell;
    private static HashMap<CompanyEnum, Integer> applicationBuy;
    private static long startTime;
    private static long tradingTime;

    static {
        applicationSell = new HashMap<>();
        applicationBuy = new HashMap<>();
        startTime = System.currentTimeMillis();
    }

    private Exchange() {
    }

    public static Exchange getInstance() {
        lockExchange.lock();
        try {
            if (instance == null) {
                instance = new Exchange();
            }
        } finally {
            lockExchange.unlock();
        }
        return instance;
    }

    public boolean spendTime() {
        return System.currentTimeMillis() - startTime < tradingTime ? true : false;
    }

    public static long getStartTime() {
        return startTime;
    }

    public static void setStartTime(long startTime) {
        Exchange.startTime = startTime;
    }

    //methods for Sell
    public Integer putApplicationSell(CompanyEnum key, Integer value) {
        try {
            lock.lock();
            return applicationSell.put(key, value);
        } finally {
            lock.unlock();
        }
    }

    public boolean containsKeySell(Object key) {
        try {
            lock.lock();
            return applicationSell.containsKey(key);
        } finally {
            lock.unlock();
        }
    }

    public Integer getSell(CompanyEnum key) {
        try {
            lock.lock();
            return applicationSell.get(key);
        } finally {
            lock.unlock();
        }
    }

    public Integer removeSell(CompanyEnum key) {
        try {
            lock.lock();
            return applicationSell.remove(key);
        } finally {
            lock.unlock();
        }
    }

    //methods for Buy
    public Integer putApplicationBuy(CompanyEnum key, Integer value) {
        try {
            lock.lock();
            return applicationBuy.put(key, value);
        } finally {
            lock.unlock();
        }
    }

    public boolean containsKeyBuy(Object key) {
        try {
            lock.lock();
            return applicationBuy.containsKey(key);
        } finally {
            lock.unlock();
        }
    }

    public Integer getBuy(CompanyEnum key) {
        try {
            lock.lock();
            return applicationBuy.get(key);
        } finally {
            lock.unlock();
        }
    }

    public Integer removeBuy(CompanyEnum key) {
        try {
            lock.lock();
            return applicationBuy.remove(key);
        } finally {
            lock.unlock();
        }
    }

    public double getIndex() {
        try {
            lock.lock();
            return index;
        } finally {
            lock.unlock();
        }
    }

    //index ia double
    public void setIndex(double index) {
        lock.lock();
        this.index = index;
        lock.unlock();
    }

    public long getTradingTime() {
        return tradingTime;
    }

    public void setTradingTime(long tradingPeriod) {
        Exchange.tradingTime = tradingPeriod;
    }
}
