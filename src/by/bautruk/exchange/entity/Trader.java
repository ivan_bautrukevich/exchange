package by.bautruk.exchange.entity;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ivan.bautruvkevich
 *         02.12.2015 19:23.
 */
public class Trader implements Runnable {

    static Logger logger = Logger.getLogger(Trader.class);

    private long id;
    private final int START_MONEY;
    private int money;
    private HashMap<CompanyEnum, Integer> traderStocks;
    private ActionType tradeStatus;
    private Exchange market;
    private CompanyEnum chosenStock;

    public Trader(Exchange market, long id, int money, HashMap<CompanyEnum, Integer> traderStocks) {
        this.market = market;
        this.id = id;
        this.money = money;
        this.traderStocks = traderStocks;
        START_MONEY = totalMoney();
    }

    public void run() {
        while (market.spendTime()) {
            chooseBuySell();
            giveStockToExchange();
            startTrade();
            resultTrading();
        }
    }

    private void chooseBuySell() {
        chosenStock = CompanyEnum.getRandom();
        getRandomBoolean();
        switch (tradeStatus) {
            case BUY:
                market.setIndex(market.getIndex() + Math.random() * 1);
                break;
            case SELL:
                while (!traderStocks.containsKey(chosenStock)) {
                    chosenStock = CompanyEnum.getRandom();
                }
                market.setIndex(market.getIndex() - Math.random() * 1);
                break;
            default:
                logger.info("Wrong tradeStatus");
        }
        logger.info("Trader №" + getId() + " will " + getTradeStatus() + ". He'll " + getTradeStatus() + " of " + chosenStock);
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            logger.error("Time exception");
        }
    }

    private void giveStockToExchange() {
        //check status
        switch (tradeStatus) {
            case BUY:
                logger.info("Trader №" + id + " give to Exchange application for buying " + chosenStock);
                //check that market has randomStock
                if (market.containsKeyBuy(chosenStock)) {
                    market.putApplicationBuy(chosenStock, market.getBuy(chosenStock) + 1);
                } else {
                    market.putApplicationBuy(chosenStock, 1);
                }
                break;
            case SELL:
                logger.info("Trader №" + id + " give to Exchange application for sailing " + chosenStock);
                //check that market has randomStock
                if (market.containsKeySell(chosenStock)) {
                    market.putApplicationSell(chosenStock, market.getSell(chosenStock) + 1);
                } else {
                    market.putApplicationSell(chosenStock, 1);
                }
                break;
            default:
                logger.info("Wrong tradeStatus");

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    logger.error("Time exception");
                }
        }
    }

    private void startTrade() {
        switch (tradeStatus) {
            case BUY:
                buy();
                break;
            case SELL:
                sell();
                break;
        }
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            logger.error("Time exception");
        }
    }

    public int resultTrading() {
        int result = totalMoney() - START_MONEY;
        logger.info("Trader №" + getId() + " has result of trading " + result);
        return result;

    }

    public int totalMoney() {
        int total = money;
        for (Map.Entry<CompanyEnum, Integer> entry : getTraderStocks().entrySet()) {
            CompanyEnum key = entry.getKey();
            Integer value = entry.getValue();
            total = total + key.getPrice() * value;
        }
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            logger.error("Time exception");
        }
        return total;
    }

    private void buy() {
        //check that market has randomStock
        if (market.containsKeySell(chosenStock)) {
            //check for the last stock
            if (market.getSell(chosenStock) != 1) {
                market.putApplicationSell(chosenStock, market.getSell(chosenStock) - 1);
            } else {
                market.removeSell(chosenStock);
            }
            logger.info("Trader №" + id + " buy from Exchange " + chosenStock);
            //check that trader has randomStock
            if (traderStocks.containsKey(chosenStock)) {
                traderStocks.put(chosenStock, traderStocks.get(chosenStock) + 1);
            } else {
                traderStocks.put(chosenStock, 1);
            }
            money -= (1 + market.getIndex()) * chosenStock.getPrice();
        } else {
            logger.info("Exchange doesn't have application for selling " + chosenStock + " for Trader №" + id);
        }
    }

    private void sell() {
        //check that market has randomStock
        if (market.containsKeyBuy(chosenStock)) {
            //check for the last stock
            if (market.getBuy(chosenStock) != 1) {
                market.putApplicationBuy(chosenStock, market.getBuy(chosenStock) - 1);
            } else {
                market.removeBuy(chosenStock);
            }
            logger.info("Trader №" + id + " sell for Exchange " + chosenStock);
            //check that trader has randomStock
            if (traderStocks.containsKey(chosenStock)) {
                traderStocks.put(chosenStock, traderStocks.get(chosenStock) - 1);
            } else {
                traderStocks.put(chosenStock, 1);
            }
            money += (1 + market.getIndex()) * chosenStock.getPrice();
        } else {
            logger.info("Exchange doesn't have application for buying " + chosenStock + " for Trader №" + id);
        }
    }

    public ActionType getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(ActionType tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    private void getRandomBoolean() {
        tradeStatus = Math.random() < 0.5 ? ActionType.BUY : ActionType.SELL;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public HashMap<CompanyEnum, Integer> getTraderStocks() {
        return traderStocks;
    }

    public void setTraderStocks(HashMap<CompanyEnum, Integer> traderStocks) {
        this.traderStocks = traderStocks;
    }

    public Integer get(Object key) {
        return traderStocks.get(key);
    }

    public Integer put(CompanyEnum key, Integer value) {
        return traderStocks.put(key, value);
    }

    @Override
    public String toString() {
        return "Trader{" +
                "id=" + id +
                ", money=" + money +
                ", traderStocks=" + traderStocks +
                '}';
    }
}
