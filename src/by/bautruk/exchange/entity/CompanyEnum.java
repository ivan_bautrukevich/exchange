package by.bautruk.exchange.entity;

/**
 * @author ivan.bautruvkevich
 *         03.12.2015 21:20.
 */
public enum CompanyEnum {

    //NameOfCompany(Nominal price)

    APPLE(1000),
    GOOGLE(500),
    TESLA(750),
    YAHOO(200),
    EPAM(250);

    private int price;

    CompanyEnum(int i) {
        price = i;
    }

    public int getPrice() {
        return price;
    }

    public static CompanyEnum getRandom(){
        return values()[(int)(Math.random()*values().length)];
    }
}
