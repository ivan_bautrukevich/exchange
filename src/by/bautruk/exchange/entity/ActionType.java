package by.bautruk.exchange.entity;

/**
 * @author ivan.bautruvkevich
 *         04.12.2015 19:22.
 */
public enum ActionType {
    BUY,
    SELL
}
