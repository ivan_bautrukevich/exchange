package by.bautruk.exchange.entity;

import java.util.HashMap;

/**
 * @author ivan.bautruvkevich
 *         06.12.2015 15:02.
 */
public class Creator {
    public HashMap<CompanyEnum, Integer> createTraderStock() {
        HashMap<CompanyEnum, Integer> traderStocks = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            traderStocks.put(CompanyEnum.getRandom(), (int) (1 + Math.random() * 5));
        }
        return traderStocks;
    }
}
